FROM alpine
ENV GOPATH='/go'
ENV PATH='/go/bin:/usr/local/go/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
RUN apk add go git openssh